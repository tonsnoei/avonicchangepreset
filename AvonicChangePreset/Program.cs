﻿using System;
using System.Net;

namespace AvonicChangePreset
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Usage: AvonicChangePreset <IP> <username> <password> <preset>");
                Console.WriteLine("Written by Ton Snoei (2020). Free for use. No warranties.");
                return;
            }

            string ip = args[0];
            string username = args[1];
            string password = args[2];
            string preset = args[3];

            CookieAwareWebClient webClient = new CookieAwareWebClient();
            Login(webClient, ip, username, password);
            ChangePreset(webClient, ip, Convert.ToInt32(preset));
        }

        private static void Login(WebClient webClient, string baseUrl, string username, string password)
        {
            string data = webClient.DownloadString($"http://{baseUrl}/login/login?username={username}&password={password}");
        }

        private static void ChangePreset(WebClient webClient, string baseUrl, int preset)
        {
            string data = webClient.DownloadString($"http://{baseUrl}/ajaxcom?szCmd=%7B%22SysCtrl%22%3A%7B%22PtzCtrl%22%3A%7B%22nChanel%22%3A0%2C%22szPtzCmd%22%3A%22preset_call%22%2C%22byValue%22%3A{preset}%7D%7D%7D");
        }
    }
}
